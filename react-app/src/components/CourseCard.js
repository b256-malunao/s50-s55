// import {useState} from 'react' /*when using the state of our code*/
import {Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

// props or properties acts as a function parameter
/*
props = courseProp : { 
    description : "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore."
    id : "wdc001"
    name : "PHP - Laravel"
    onOffer : true
    price : 45000
}
*/
/*
export default function CourseCard(props) {

	console.log(props);
	console.log(typeof props);
	return (
		 <Card>
	      <Card.Body>
	        <Card.Title>{props.courseProp.name}</Card.Title>
		          <Card.Subtitle>Description:</Card.Subtitle>
		          <Card.Text>{props.courseProp.description}</Card.Text>
		          <Card.Subtitle><strong>Price:</strong></Card.Subtitle>
		          <Card.Text>{props.courseProp.price}</Card.Text>
		      <Button variant="primary">Enroll</Button>
	      </Card.Body>
	    </Card>

	)
}
*/

/*export default function CourseCard({courseProp}) {

	// console.log(props);
	// console.log(typeof props);

	return (
		 <Card>
	      <Card.Body>
	        <Card.Title>{courseProp.name}</Card.Title>
		          <Card.Subtitle>Description:</Card.Subtitle>
		          <Card.Text>{courseProp.description}</Card.Text>
		          <Card.Subtitle><strong>Price:</strong></Card.Subtitle>
		          <Card.Text>{courseProp.price}</Card.Text>
		      <Button variant="primary">Enroll</Button>
	      </Card.Body>
	    </Card>

	)
}*/

export default function CourseCard({courseProp}) {

	// console.log(props);
	// console.log(typeof props);
	
	// Object Deconstruction
	const { name, description, price, _id } = courseProp;

	// getter => it stores the value. variable
	// setter => it sets the value to be stored in the getter
	// nagagamit si useState dahil inimport
	// (0) is the initial Getter Value
	// const [count, setCount] = useState(0);
	// const [seats,setSeats] = useState(30);


/*function enroll() {

	  if (seats > 0) {
            setCount(count + 1);
            console.log('Enrollees: ' + count);
            setSeats(seats - 1);
            console.log('Seats: ' + seats);

        } else {
            alert("No more seats available");
        };
    */


		// matitrigger kapag gumagana si Enroll section/ kapag clinick yung Enroll Button
	/*	setCount(count + 1);
		console.log('Enrollees: ' + count)


		setSeat(seat - 1);
		console.log('Seats available: ' + seat)
			if(seat === 0 && count === 30) {
			alert("No more seats");
	
	} 


 
}*/

	return (
		 <Card>
	      <Card.Body>
	        <Card.Title>{name}</Card.Title>
		          <Card.Subtitle>Description:</Card.Subtitle>
		          <Card.Text>{description}</Card.Text>
		          <Card.Subtitle><strong>Price:</strong></Card.Subtitle>
		          <Card.Text>{price}</Card.Text>
		          {/*<Card.Subtitle><strong>Enrollees:</strong></Card.Subtitle>*/}
		          {/*<Card.Text>{count} students</Card.Text>*/}
		          {/*<Card.Subtitle><strong>Seats:</strong></Card.Subtitle>*/}
		          {/*<Card.Text>{seats} seats</Card.Text>*/}
		      <Button variant="primary" as={Link} to={`/courseView/${_id}`}>Details</Button>
	      </Card.Body>
	    </Card>

	)
}