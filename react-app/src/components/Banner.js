/*// destructuring react bootstrap
import { Button, Row, Col } from 'react-bootstrap';

export default function Banner() {
	return(
		
	
		<Row>
			<Col className="p-5 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
		


	)
}*/

// destructuring react bootstrap components
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const {title, content, destination, label} = data;
    console.log(data);

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>
	)
}
