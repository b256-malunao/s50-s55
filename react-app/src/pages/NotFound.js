/*import { Button, Row, Col } from 'react-bootstrap';


export default function Banner() {
	return(
		
	
		<Row>
			<Col>
				<h1>Page Not Found</h1>
				<p>Go back to the <a href="/">homepage</a>.</p>
				
			</Col>
		</Row>
		


	)
}*/

import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}

