// import CourseData from '../data/CourseData';
import { useState, useEffect } from 'react';
import CourseCard from '../components/CourseCard';

// export default function Courses() {

// 	console.log(CourseData);

// 	const courses = CourseData.map(course => {

// 		return (
// 			<CourseCard key={course.id} courseProp={course} /> /*key->unique identifier*/
// 		)
// 	})

// 	return(
// 		<>
// 			<h1>Courses</h1>
// 			{/*<CourseCard courseProp={CourseData[0]} /> */} {/*{} - value*/}
// 			{courses}
// 		</>
// 	)
// } 

export default function Courses() {

	const [ courses, setCourses ] = useState([]);

	useEffect(() => {

		fetch('http://localhost:4000/courses/active')
		.then(res => res.json())
		.then(data => {

			// console.log(data);

			setCourses(data.map(course => {
				return (
					<CourseCard key={course.id} courseProp={course} /> /*key->unique identifier*/
				)
		}))
	})
})

	return(
		<>
			<h1>Courses</h1>
			{/*<CourseCard courseProp={CourseData[0]} /> */} {/*{} - value*/}
			{courses}
		</>
	)
} 
