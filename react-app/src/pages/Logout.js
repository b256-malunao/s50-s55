import { useContext, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';


export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);
	
	unsetUser();
	// localStorage.clear();

	useEffect(() => {

		setUser({

			// email: null
			id: null

		})
	})

	return(

		<Navigate to="/login" />

	)
}